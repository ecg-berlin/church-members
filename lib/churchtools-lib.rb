require 'faraday'
require 'faraday/net_http'
require 'faraday_middleware'
require 'faraday-cookie_jar'
require 'tzinfo'
require 'digest/md5'
Faraday.default_adapter = :net_http


def ct_init()
    # see https://ecghellersdorf.church.tools/api
    $ct = Faraday.new(url: $config['churchtools']['url'], headers: { 'Content-Type' => 'application/json'}) do |faraday|
        faraday.use Faraday::Response::RaiseError
        faraday.use :cookie_jar
        faraday.request :json # encode req bodies as JSON and automatically set the Content-Type header
        faraday.response :json # decode response bodies as JSON
        faraday.adapter :net_http
    end
    
    $ct.get('?q=login/ajax&func=loginWithToken&token='+$config['churchtools']['login_token'])
end

def cached_get(path, params={})
    val = [path, params].to_json
    hash =  Digest::MD5.hexdigest(val)
    if(File.exists?('cache/'+hash+".json")) 
        return Marshal.load(File.read('cache/'+hash+".json"))
    end
    ret = $ct.get(path, params) 
    File.write('cache/'+hash+".json", Marshal.dump(ret))
    return ret
end

def birthdays(start_date, end_date)
    response = $ct.get('api/persons/birthdays', {start_date: start_date.to_s, end_date: end_date.to_s}) 
    response.body["data"]
end

def events(start_date, end_date)
    response = $ct.get('api/events', {from: start_date.to_s, to: (end_date+1).to_s}) 
    response.body["data"]
end


def get_people()
    response = cached_get('api/persons', {status: [3], is_archived: false, page: 1, limit: 200}) 
    response.body["data"]
end

def get_groups_by_person(peroson_id)
    response = cached_get("api/persons/#{peroson_id}/groups") 
    response.body["data"]
end