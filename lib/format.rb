def format_phone(number)
    if number.start_with? '49'
      number.slice!(0, 2)
      number = '0'+number
    end
    prefixes = [
        "030", "03342", "03341" "01511", "01512", "01514", "01515", "01516",
        "01517", "0160", "0170", "0171", "0175", "01520", "01522", "01523",
        "01525", "0162", "0172", "0173", "0174", "01570", "01573", "01575",
        "01577", "01578", "0163", "0177", "0178", "01590", "0176", "0179"
    ]
    found_prefix = nil
    prefixes.each do |prefix|
      if number.start_with? prefix
        found_prefix = prefix
      end
    end
  
    if found_prefix.nil?
      return number
    else
      number.slice!(0, found_prefix.length)
      return found_prefix + " " + number
    end
  end
  
  
  def age(birthday)
    (DateTime.now - birthday) / 365.25
  end