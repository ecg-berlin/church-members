require 'pp'
require 'date'
require 'elvanto'

# https://www.elvanto.com/api/people/getAll/
def get_people(category_id, page=1, page_size=1000)
  people = ElvantoAPI.call("people/getAll", {
      "page" => 1,
      "page_size" => page_size,
      "category_id" => category_id,
      "archived" => "no",
      "deceased" => "no",
      "fields" => [
          "gender",
          "birthday",
          "home_address",
          "home_city",
          "home_postcode",
          "home_country",
          "home_state",
          "marital_status",
          "departments",
          "family",
          "anniversary",
          $config['extra']['taufe']
      ]
  })
  p = people['people']['person']
  return p
end

def get_people_from_group(group_id)
  people = ElvantoAPI.call("groups/getInfo", {
      "id" => group_id,           
      "fields" => [
          "people",
      ]
  })
  p = people['group'][0]['people']['person']
  return p
end

def extended_data(people:, disable_children_age:)
  top_persons_ids = {}
  people.each do |person|
    top_persons_ids[person['id']] = person['id']
  end
  people.map do |person|
    person["groups"] = []

    if not person['mobile'].empty?
      person['mobile'] = format_phone(person['mobile'])
    end

    if not person['phone'].empty?
      person['phone'] = format_phone(person['phone'])
    end

    begin
      birthday = Date.parse(person['birthday'])
      person['birthday'] = birthday.strftime('%d.%m.%Y')
      person['age'] = age(birthday)
    rescue
      person['birthday'] = ''
      person['age'] = nil
    end

    begin
      taufe = Date.parse(person[$config['extra']['taufe']])
      person['taufe'] = taufe.strftime('%d.%m.%Y')
    rescue
      person['taufe'] = ''
    end

    if person["family"].any?
      children = person["family"]["family_member"].select {|member| member['relationship'] == 'Child' and not top_persons_ids.key? member['id']}
      if children.any? && person["family_relationship"] != "Child"
        person['childrenData'] = children_data(disable_children_age, children)
        person['genChildren'] = children_string(person['childrenData'])
      end

    end
    person
  end
  return addGroups(people)
end

def children_string(children)
  children.map do |child|
    if child[:age].nil?
      child[:name]
    else
      child[:name] + ' (' + child[:age].to_s + ')'
    end
  end.join (", ");
end

def children_data(disable_children_age, children)
  children_cache = {}
  children.map do |child|
    if disable_children_age
      {:name => child['firstname'], :age => nil, :birthday => nil}
    else
      if children_cache.has_key? child["id"]
        children_cache[child["id"]]
      else
        data = ElvantoAPI.call("people/getInfo", {
            "id": child["id"],
            "fields": [
                "birthday",
            ]
        })
        child_info = data["person"][0]
        birthday = child_info['birthday']
        if birthday.empty?
          children_cache[child["id"]] = {:name => child['firstname'], :age => nil, :birthday => nil}
        else
          bday = Date.parse(birthday)
          children_cache[child["id"]] = {:name => child['firstname'], :age => age(bday).floor, :birthday => bday}
        end
        children_cache[child["id"]]
      end
    end
  end
end

def addGroups(people)

  groups = ElvantoAPI.call("groups/getAll", {
      "page" => 1,
      "page_size" => 1000,
      "category_id" => $config['dienst_category_id'],
      "fields" => [
          "people",
          "departments",
      ]
  })

  groups = groups["groups"]["group"]

  people.map do |person|
    next if not person["departments"].any?
    departments = person["departments"]["department"].map {|x| x['name']}
    person["genDepartments"] = departments.join(", ");
  end


  people.map do |person|
    myGroups = groups.select {|group| group["people"].respond_to?('has_key?') and group["people"]["person"].any? {|p| p["id"] == person["id"]}}
    person["genGroups"] = myGroups.map {|x| x['name']}.join(", ")
  end
  people
end



