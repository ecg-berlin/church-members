require 'yaml'
require 'handlebars'
require 'pp'
require 'optparse'
require_relative '../lib/elvanto-lib'

$config = YAML.load_file('../secrets.yml')

def group_by_family(persons)
  families = {}
  persons.each do |person|
    if not person['family_id'] == ""
      if families.has_key? person['family_id']
        families[person['family_id']] << person
      else
        families[person['family_id']] = [person]
      end
    end
  end

  families = sort_by_family(families)
  ret = {}
  persons.each do |person|
    if person['family_id'] == ""
      ret[person['lastname'] + '_' + person['firstname'] + '_' + person['birthday']] = person
    end
  end
  families.each do |family|
    first = family[0]
    ret[first['lastname'] + '_' + first['firstname'] + '_' + first['birthday']] = family
  end
  keys = ret.keys.sort

  sorted = []
  keys.each do |key|
    sorted << ret[key]
  end
  sorted.flatten
end


def sort_by_family(families)
  ret = []
  families.each do |key, family|
    family.sort_by! { |person| person['birthday']}
    if(family.length > 1)
      first = family[0].dup
      second = family[1].dup
      if first["gender"] == "Female" and second["gender"] = "Male"
        family[0] = second
        family[1] = first
      end
    end
    ret << family
  end
  return ret
end


def main()
  options = {
      :template => './template.html',
      :outfile => 'out.html',
      :cache => nil
  }


  OptionParser.new do |opts|
    opts.banner = "Usage: create.rb [options]"

    opts.on("-t PATH", "--template PATH", "Path for template file") do |v|
      options[:template] = v
    end

    opts.on("-o PATH", "--out PATH", "Filename for results") do |v|
      options[:outfile] = v
    end

    opts.on("-n", "--disable-children-age", "Do not calculate (helpful for test runs)") do |v|
      options[:disableChildrenAge] = v
    end

    opts.on("-c", "--cache", "Cache file") do |v|
      options[:cache] = v
    end
  end.parse!

  if options[:cache] != nil and File.exists? options[:cache]
    data = YAML.load(File.read(options[:cache]))
  else
    data = nil
  end

  if(data === nil) 
    ElvantoAPI.configure({:api_key=>$config['api_key']})
    mitglieder = extended_data(people: group_by_family(get_people($config['category_id'])), disable_children_age: options[:disableChildrenAge])
    data = {
      'people' => mitglieder,
    }
    if options[:cache] != nil
      File.open(options[:cache], "w") { |file| file.write(data.to_yaml) }
    end
  end
  handlebars = Handlebars::Context.new
  template = handlebars.compile(File.read(options[:template]))
  File.write(options[:outfile],  template.call(data))
end
main()
