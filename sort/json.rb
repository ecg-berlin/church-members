require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
require 'json'
$config = YAML.load_file('../secrets.yml')

def getAllPeople
  people = ElvantoAPI.call("people/getAll", {
      "page" => 1,
      "page_size" => 1000,
      "category_id" => $config['category_id'],
      "fields" => [
          "gender",
          "birthday",
          "home_address",
          "home_city",
          "home_postcode",
          "marital_status",
      ]
  })

  p = people['people']['person']


  return p
end


def is_between(name, start, finish)
  name[0].downcase >= start.downcase and name[0].downcase <= finish.downcase
end

def main()
  ElvantoAPI.configure({:api_key => $config['api_key']})
  start = 'l'
  finish = 'r'


  people = getAllPeople()
  people = people.select { |people| is_between(people['lastname'], start, finish) }
  people = people.sort_by { |a| a['lastname'] }

  json = []
  people.each do |person|
    puts person['lastname'] + ' ' + person['firstname']
    json << {'name' =>  person['firstname'] + ' ' + person['lastname'], 'mobile' => person['mobile'], 'phone' => person['phone'], 'email' => person['email']};
  end

  puts people.length
  
  File.open('data.json', 'w') { |file| file.write(json.to_json) }

end
main()
