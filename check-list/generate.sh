#!/usr/bin/env bash
set -e
compass compile
ruby create.rb
wkhtmltopdf --enable-local-file-access --margin-bottom 10mm --margin-left 10mm --margin-right 10mm --margin-top 10mm --page-size A5  out.html out.pdf
