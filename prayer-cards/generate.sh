#!/usr/bin/env bash
set -e
compass compile
ruby create.rb
wkhtmltopdf --enable-local-file-access --margin-bottom 0mm --margin-left 0mm --margin-right 0mm --margin-top 0mm --page-height '297mm' --page-width '210mm' out.html out.pdf
