require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
require 'optparse'
require '../lib/elvanto-lib'

def main
  $config = YAML.load_file('../secrets.yml')
  handlebars = Handlebars::Context.new

  front = handlebars.compile(File.read('./front.html'))
  back = handlebars.compile(File.read('./back.html'))
  page_front = handlebars.compile(File.read('./page_front.html'))
  page_back = handlebars.compile(File.read('./page_back.html'))


  ElvantoAPI.configure({:api_key => $config['api_key']})
  mitglieder = extended_data(people: get_people($config['category_id'], 1, 1000), disable_children_age: true)
  people = mitglieder

  #teenie = get_people_from_group('fa83db31-e539-11e6-8f05-0a6b0d448233')
  #people = teenie

  #jugend = get_people_from_group('4433d8ee-e5ad-11e6-8f05-0a6b0d448233')
  #people = jugend
  people.sort_by! { |person| person['lastname']}

  content = ""

  i = 0
  people.each_slice(8) do |persons|
    fronts = []
    backs = []
    persons.each do |person|
      person['counter'] = (i+1)
      i += 1
      fronts << front.call({"person" => person})
      backs << back.call({"person" => person})
    end
    content += page_front.call({"card" => fronts})
    content += page_back.call({"card" => backs})

  end
  template = handlebars.compile(File.read('./template.html'))
  File.write("out.html", template.call({'content' => content}))
end

main
