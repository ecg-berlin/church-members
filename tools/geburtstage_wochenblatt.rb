require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
require 'time'
require_relative '../lib/elvanto-lib'
$config = YAML.load_file('./secrets.yml')

def main()
  ElvantoAPI.configure({:api_key => $config['api_key']})
  people = get_people($config['category_id'])
  people = people.concat(get_people($config['missionary_category_id']))

  people = people.sort_by { |a| a['lastname'] }
  people.each do |person|
    if(person['birthday'].empty?)
        #puts 'KEIN GEBURTSTAG:' + person['firstname'] + ',' + person['lastname']
        next
    end
    birthday = Date.parse(person['birthday'])
    puts person['lastname'] + '/' + person['firstname'] + '/' + birthday.strftime("%d.%m.%Y")
  end
end
main()
