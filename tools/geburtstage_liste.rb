require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
require 'time'
require_relative '../lib/elvanto-lib'
$config = YAML.load_file('./secrets.yml')

def main()
  ElvantoAPI.configure({:api_key => $config['api_key']})
  people = get_people($config['category_id'])
  people = people.sort_by do |a|
    if(a['birthday'].empty?)
        0
    else
        Date.parse(a['birthday']).yday
    end
  end
  people.each do |person|
    if(person['birthday'].empty?)
        puts "00.00.0000" + ' ' + person['firstname'] + ' ' + person['lastname']
        next
    end
    birthday = Date.parse(person['birthday'])
    puts birthday.strftime("%d.%m.%Y") + ' ' + person['firstname'] + ' ' + person['lastname']
  end

end



main()
