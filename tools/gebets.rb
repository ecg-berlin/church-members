require 'yaml'
require 'elvanto'
require 'handlebars'
require 'pp'
$config = YAML.load_file('./secrets.yml')

def getAllPeople
  people = ElvantoAPI.call("people/getAll", {
      "page" => 1,
      "page_size" => 1000,
      "category_id" => $config['category_id'],
      "fields" => [
          "gender",
          "birthday",
          "home_address",
          "home_city",
          "home_postcode",
          "marital_status",
      ]
  })

  p = people['people']['person']


  return p
end


def main()
  ElvantoAPI.configure({:api_key => $config['api_key']})
  start = 'a'
  finish = 'f'

  people = getAllPeople()
  people = people.select { |people| people['lastname'].downcase >= start and people['lastname'].downcase <= finish }
  people = people.sort_by { |a| a['lastname'] }

  people.each do |person|
    puts person['firstname'] + ' ' + person['lastname']
    puts person['email'] if person['email'] != ''
    puts person['mobile'] if person['mobile'] != ''
    puts person['phone'] if person['phone'] != ''
    puts ""

  end

end

def test
  people.each do |person|
    i += 1
    peopleGroups[pastors[i % pastors.length - 1]] << person
  end
  peopleGroups.each do |key, group|
    puts key
    puts ""
    group.each do |person|
      puts person['firstname'] + ' ' + person['lastname']
      puts person['email'] if person['email'] != ''
      puts person['mobile'] if person['mobile'] != ''
      puts person['phone'] if person['phone'] != ''
      puts ""
    end
    puts '-----------------'

  end
end

main()
