require 'yaml'
require 'elvanto'
require 'pp'
require 'vcardigan'
require 'date'

require_relative '../lib/elvanto-lib'
def main()
    $config = YAML.load_file('secrets.yml')
    ElvantoAPI.configure({:api_key=>$config['api_key']})
    people = get_people($config['category_id'])
    people = people.concat(get_people($config['besucher_category_id']))
                           
    current_file = ""

    people.each do |person|
      vcard = VCardigan.create
      vcard.name person['lastname'], person['firstname']
      vcard.fullname person['firstname'] + ' ' + person['lastname']

      if not person['email'].empty?
        vcard.email person['email'], :preferred => 1
      end

      if not person['phone'].empty?
        vcard.homePhone person['phone']
      end

      if not person['mobile'].empty?
        vcard.cellPhone person['mobile']
      end

      if not person['birthday'].empty?
        birthday = Date.parse(person['birthday'])
        vcard.bday birthday.strftime('%Y-%m-%d')
      end

      if not person['anniversary'].empty?
        vcard.anniversary person['anniversary']
      end

      if not person['home_address'].empty?
        vcard.adr('', '', person['home_address'], person['home_city'], person['home_state'], person['home_postcode'], person['home_country'])
      end
      
      if not person['picture'].include? 'default'
        vcard.photo person['picture'], :type => 'uri'
      end
      
      current_file += "\n"
      current_file += vcard.to_s

    end
    File.write('card.vcf', current_file)

end
main()

