# Church-Members Tool Box

![Logo](logo.png "Logo")

This is an tool box for working with ChurchTools

## Installation

1. Install ruby (including development headers). It does work with ruby 3.0. `sudo apt install ruby ruby-dev`

2. Install some modules for ruby. `sudo gem install vcardigan pp handlebars compass rubocop` 

3. Install [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html). The patched version wkhtmltopdf is needed.

4. Create the config file `./secrets.yml`

```yml
---
api_key: xxxxxxxxxxxxxxx
category_id: "xxx-xxx-xxx-xxx"
missionary_category_id: "xxx-xxx-xxx-"
dienst_category_id: "xxx-xxx-xxx--xx"
besucher_category_id: "xxx-xxx"
extra:
  taufe: "custom_…"
long_term_bedridden:
 - xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx # frau mustermann
 - xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxx # herr mustermann
```

## The big and important functions

### Membership Directory

Creates an pdf with every member in the church.

```bash
cd membership-directoy
./generate.sh
```

### Check-List

Creates an corona check list

```bash
cd check-list
./generate.sh
```


### Prayer Cards

Creates A7 Cards for prayer meetings.

```bash
cd prayer-cards
./generate.sh
```


## Tools

### Birthdays

Gibt ein Liste mit den Geburtstagsliste aus

```bash
ruby tools/geburtstage.rb > list.txt
```


### Generate messages to members

Um Daten zu prüfen:

```
ruby check_informations.rb > ~/check.txt
```
