require 'yaml'
require 'pp'
require 'optparse'
require_relative '../lib/churchtools-lib'

$config = YAML.load_file('../secrets.yml')


def main()

  options = {
    :groupTypes => [15]
  }


  OptionParser.new do |opts|
    opts.banner = "Usage: create-graph.rb [options]"

    opts.on("-t PATH", "--template PATH", "Path for template file") do |v|
      options[:template] = v
    end

    opts.on("-o PATH", "--out PATH", "Filename for results") do |v|
      options[:outfile] = v
    end
    opts.on("-c PATH", "--cache PATH", "Cache file") do |v|
      options[:cache] = v
    end
  end.parse!

  ct_init();

  
  dot_output = "digraph {"
  people = get_people()

  people.each do |person|
    name = person["firstName"] + " " + person["lastName"]
    pp name
    groups = get_groups_by_person(person["id"])
    groups.each do |group|
      next unless options[:groupTypes].include? group["groupTypeRoleId"]
      dot_output << '"' + group["group"]["title"] + '" -> ' + '"' + name + '"' + "\n"
    end
    if groups.length == 0
      dot_output << '"' + name + '"' + "\n"
    end

  end


  dot_output << "}"

  File.write("log.dot", dot_output)
  `dot -Tsvg log.dot > ret.svg`



end
main()
